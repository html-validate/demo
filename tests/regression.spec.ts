import path from "path";
import { CLI } from "html-validate";
import { globSync } from "glob";

const patterns = ["simple/**/*.html", "extending/**/*.html", "showcase/**/*.html"];
const rootDir = path.join(__dirname, "..");
const files = patterns
	.map((it) => globSync(path.join(rootDir, it)))
	.flat()
	.map((it) => path.relative(rootDir, it));

const cli = new CLI();

it.each(files)("%s", async (filename) => {
	expect.assertions(1);
	const htmlvalidate = await cli.getValidator();
	const report = await htmlvalidate.validateFile(filename);
	expect(report).toMatchSnapshot();
});
