# Basic demo files

Short and simple demo files showcasing complexities of HTML5 and why validation is needed.

## `<p>` vs `<address>`

Demonstrates the complexities of the `<p>` tag, specifically the concept of implicitly closed elements.
