# `<p>` vs `<address>`

Demonstrates the complexities of the `<p>` tag, specifically the concept of implicitly closed elements.
