# Extending HTML-Validate

Shows how to extend HTML-Validate with new rules, elements, configs and plugins.

- `custom-rule` shows how to write a new rule.
- `custom-element` shows how to write metadata for a new element.
- `config-preset` shows how to create and use a sharable configuration preset.
- `custom-plugin` shows how to write a plugin.
