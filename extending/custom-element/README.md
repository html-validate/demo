# Custom element

This demo shows how to add metadata for a new custom element (e.g. a native Web Component or a component from a javascript framework).

## `elements.json`

The metadata for the new element.
It declares the `<my-message-box>` element as belonging to the flow category, only accepting content from the phrasing category and the required attribute `type`.

## `.htmlvalidate.json`

The configuration required to use the new metadata.

```diff
 {
   "extends": ["html-validate:recommended"],
-  "elements": ["html5"]
+  "elements": ["html5", "./elements.json"]
 }
```
