const { Rule } = require("html-validate");

/**
 * @typedef {import("html-validate").DOMNode} DOMNode
 */

/**
 * Determine if node is a text node.
 *
 * @param {DOMNode} node
 * @returns {boolean}
 */
function isTextNode(node) {
	return node && node.nodeName === "#text";
}

/**
 * Test if "lorem ipsum" is present in the text.
 *
 * @param {DOMNode} node
 * @returns {DOMNode | undefined}
 */
function haveLoremIpsum(node) {
	const text = node.textContent.toLowerCase();
	return text.includes("lorem ipsum") ? node : undefined;
}

class NoLoremIpsum extends Rule {
	/**
	 * Rule initialization.
	 */
	setup() {
		/* setup an event listener for when an element and all its children are ready */
		this.on("element:ready", (event) => {
			const { target } = event;

			/* filter children so we only work on the text nodes */
			const textNodes = target.childNodes.filter((it) => isTextNode(it));

			/* for each given text node we test if the "lorem ipsum" text is
			 * present and report an error if it exists */
			for (const node of textNodes) {
				const invalid = haveLoremIpsum(node);
				if (!invalid) {
					continue;
				}
				this.report(node, "Must not have lorem ipsum as text", invalid.location);
			}
		});
	}
}

module.exports = NoLoremIpsum;
