# Custom rule

This demo shows how to write and use a custom rule.

## `no-lorem-ipsum.js`

Rule implementation.

## `plugin.js`

Simple plugin to expose the rule.

## `.htmlvalidate.json`

The configuration required to use and enable the rule (via plugin).

```diff
 {
   "extends": ["html-validate:recommended"],
   "elements": ["html5"]
+  "plugins": ["./plugin"]
+  "rules": {
+    "demo/no-lorem-ipsum": "error"
+  }
 }
```
