const NoLoremIpsum = require("./no-lorem-ipsum");

module.exports = {
	rules: {
		"demo/no-lorem-ipsum": NoLoremIpsum,
	},
};
